import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  iconImage : {
    height: 25,
    width: 20,
    marginRight: 17,
    marginTop: 25
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: "column"
  },
  subMenu: {
    height: 83,
    backgroundColor: "white",
    marginLeft: 20,
    marginRight: 20,
    marginTop: 20,
    borderRadius: 10,
    elevation: 5,
    justifyContent: "center",
    flexDirection: "row"
  },

  // Icon Menu
  menuIcon: {
    height: 50, 
    width: 66,
    flexDirection: "column", 
    alignItems: "center",
    justifyContent: "center",
    margin: 6,
    marginTop: 40
  },

  // Navigation
  navIcon: {
    height: 22,
    width: 22
  },
  navIconScan: {
    height: 60, 
    width: 60, 
    borderColor: 'white', 
    borderWidth: 5, 
    borderRadius: 50,
    marginBottom: 30
  },
  navTextSelected: {
    fontSize:10,
    fontWeight:'bold', 
    color:'#49268C'
  }
});

export default styles;