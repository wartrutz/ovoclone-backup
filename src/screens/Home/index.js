import React, { Component } from 'react'
import { ScrollView, View, Image, Text, TouchableOpacity } from 'react-native'
import styles from '../../styles'

export default class Home extends Component {
  render() {
    return (
        /* All view */
        <View style={{flexDirection: "column", flex: 1, backgroundColor: "ffffff"}}>
          
            {/* Header with fix scroller */}
            <View style={{flexDirection: "row", height: 60, backgroundColor: '#4f2494'}}>
                <Text style={{marginLeft: 17, marginTop: 20, fontSize: 25, fontWeight: "bold", color: "white", flex: 1}}>OVO</Text>
                <Image source={require("../../assets/icons/bell.png")} style={styles.iconImage}></Image>
            </View>

            {/* Main page with scroller view */}

            <ScrollView style={{flexDirection: "column", flex: 1, backgroundColor: "ffffff"}}>

                {/* OVO info value and sub menu */}

                <View style={{backgroundColor: '#4f2494', height:170, borderBottomEndRadius: 40, borderBottomStartRadius: 40, flex: 1}}>
                    <Image source={require('../../assets/icons/path.png')} style={{width: '100%', height: 180, position: 'absolute'}} />
                    <Text style={{fontSize: 15, color: "#cccc", marginTop: 10, marginLeft: 19, fontWeight: "bold"}}>OVO Cash</Text>
                    <View style={{flexDirection: "row"}}>
                        <Text style={{fontSize: 18, color: "white", marginTop: 5, marginLeft: 19, fontWeight: "bold"}}>Rp</Text>
                        <Text style={{fontSize: 25, color: "white", marginTop: 5, marginLeft: 3, fontWeight: "bold"}}>0</Text>
                    </View>
                    <View style={{flexDirection: "row"}}>
                        <Text style={{fontSize: 13, color: "#cccc", marginTop: 5, marginLeft: 19, fontWeight: "bold"}}>OVO Points</Text>
                        <Text style={{fontSize: 13, color: "yellow", marginTop: 5, marginLeft: 4, fontWeight: "bold"}}>0</Text>
                    </View>

                    {/* OVO menu top up, transfer, history */}
                    
                    <View style={styles.subMenu}>
                        
                        <View style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate("TopUp")}>
                                <Image source={require("../../assets/icons/add-button.png")} style={{height: 25, width: 25, marginBottom: 5 }}></Image>
                            </TouchableOpacity>
                            <Text style={{color: '#49268C', fontWeight: "bold", fontSize: 12}}>Top Up</Text>
                        </View>

                        <View style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate("Transfer")}>
                                <Image source={require("../../assets/icons/upload.png")} style={{height: 25, width: 25, marginBottom: 5 }}></Image>
                            </TouchableOpacity>
                            <Text style={{color: '#49268C', fontWeight: "bold", fontSize: 12}}>Transfer</Text>
                        </View>

                        <View style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate("History")}>
                                <Image source={require("../../assets/icons/time.png")} style={{height: 25, width: 25, marginBottom: 5 }}></Image>
                            </TouchableOpacity>
                            <Text style={{color: '#49268C', fontWeight: "bold", fontSize: 12}}>History</Text>
                        </View>
                    </View>
                </View>
        
                {/* OVO main features */}

                <View style={{marginTop: 5, marginBottom: 30, marginLeft: 15, marginRight: 15, flex: 1, flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-between'}}>
                    <View style={{flexDirection: "row", alignItems: 'center', justifyContent: 'space-between'}}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('PLN')}>
                            <View style={styles.menuIcon}>
                                <Image source={require('../../assets/icons/flash.png')} style={{height: 45, width: 45, marginTop: 30}}></Image>
                                <Text style={{marginTop: 8, fontSize: 12, textAlign: 'center'}}>PLN</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{flexDirection: "row", alignItems: 'center', justifyContent: 'space-between'}}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Pulsa')}>
                            <View style={styles.menuIcon}>
                                <Image source={require('../../assets/icons/mobile.png')} style={{height: 45, width: 45, marginTop: 30}}></Image>
                                <Text style={{marginTop: 8, fontSize: 12, textAlign: 'center'}}>Pulsa</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{flexDirection: "row", alignItems: 'center', justifyContent: 'space-between'}}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Voucher')}>
                            <View style={styles.menuIcon}>
                                <Image source={require('../../assets/icons/game.png')} style={{height: 45, width: 45, marginTop: 30}}></Image>
                                <Text style={{marginTop: 3, fontSize: 11, textAlign: 'center'}}>Voucher Game</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{flexDirection: "row", alignItems: 'center', justifyContent: 'space-between'}}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('PDAM')}>
                            <View style={styles.menuIcon}>
                                <Image source={require('../../assets/icons/drop.png')} style={{height: 45, width: 45, marginTop: 30}}></Image>
                                <Text style={{marginTop: 8, fontSize: 12, textAlign: 'center'}}>PDAM</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{flexDirection: "row", alignItems: 'center', justifyContent: 'space-between'}}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('BPJS')}>
                            <View style={styles.menuIcon}>
                                <Image source={require('../../assets/icons/shield.png')} style={{height: 45, width: 45, marginTop: 30}}></Image>
                                <Text style={{marginTop: 8, fontSize: 12, textAlign: 'center'}}>BPJS</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{flexDirection: "row", alignItems: 'center', justifyContent: 'space-between'}}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Internet')}>
                            <View style={styles.menuIcon}>
                                <Image source={require('../../assets/icons/monitor.png')} style={{height: 45, width: 45, marginTop: 30}}></Image>
                                <Text style={{marginTop: 3, fontSize: 11, textAlign: 'center'}}>Internet & TV Kabel</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{flexDirection: "row", alignItems: 'center', justifyContent: 'space-between'}}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Proteksi')}>
                            <View style={styles.menuIcon}>
                                <Image source={require('../../assets/icons/umbrella.png')} style={{height: 45, width: 45, marginTop: 30}}></Image>
                                <Text style={{marginTop: 8, fontSize: 12, textAlign: 'center'}}>Proteksi</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{flexDirection: "row", alignItems: 'center', justifyContent: 'space-between'}}>
                        <TouchableOpacity onPress={() => alert('Not Found')}>
                            <View style={styles.menuIcon}>
                                <Image source={require('../../assets/icons/menu.png')} style={{height: 45, width: 45, marginTop: 30}}></Image>
                                <Text style={{marginTop: 8, fontSize: 12, textAlign: 'center'}}>Lainnya</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={{flex:1, backgroundColor: '#ededed', height: 13, marginTop: 20}}></View>
        
                {/* OVO Promo & lainnya */}

                <View style={{flex:1, flexDirection: "column", justifyContent:"space-between"}}>
                    <View style={{flex:1, flexDirection: "row", alignItems: "center", justifyContent: "space-between"}}>
                        <Text style={{marginLeft: 19, marginTop: 23, fontSize: 18, fontWeight: "bold"}}>Info dan Promo Spesial</Text>
                        <Text style={{marginRight: 19, marginTop: 23, fontSize: 15, fontWeight: "bold", color: "#04C5CC"}}>Lihat Semua</Text>
                    </View>
                </View>
                <ScrollView horizontal={true} style={{flex:1}}>
                    <View style={{flex:1, marginStart: 20, marginTop: 30, marginEnd: 5, height: 155, width: 325, borderRadius: 10}}>
                        <Image source={require("../../assets/dummy/27041.jpg")} style={{borderRadius:10, height: '100%', width: '100%'}} resizeMode="stretch"></Image>
                    </View>
                    <View style={{marginStart: 1, marginTop: 30, marginEnd: 5, height: 155, width: 325, borderRadius: 10}}>
                        <Image source={require("../../assets/dummy/27042.jpeg")} style={{borderRadius:10, height: '100%', width: '100%'}} resizeMode="stretch"></Image>
                    </View>
                    <View style={{marginStart: 1, marginTop: 30, marginEnd: 20, height: 155, width: 325, borderRadius: 10}}>
                        <Image source={require("../../assets/dummy/27043.jpeg")} style={{borderRadius:10, height: '100%', width: '100%'}} resizeMode="stretch"></Image>
                    </View>
                </ScrollView>
                
                <View style={{flex:1, backgroundColor: '#ededed', height: 13, marginTop: 20}}></View>
    
                {/* About OVO & Help*/}

                <View style={{flex:1, flexDirection: "column"}}>
                    <View style={{flex:1, flexDirection: "row"}}>
                        <Text style={{marginStart: 23, marginTop: 23, fontSize: 18, fontWeight: "bold"}}>Yang Menarik di OVO</Text>
                    </View>
                    <View style={{flex:1, flexDirection: "column", justifyContent: "center"}}>
                        <Text style={{marginTop: 10, marginLeft: 20, color: 'grey'}}>Jangan ngaku update kalau belum coba fitur ini</Text>
                    </View>

                    {/* OVO help menu */}

                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View style={{flex:1, borderRadius: 10, elevation: 2, marginTop: 20, marginStart: 23, margin: 5, height: 250, width: 150, backgroundColor: 'white'}}>
                            <Image source={require('../../assets/dummy/cs.jpg')} style={{height: 100, width: "100%", borderTopLeftRadius: 10, borderTopRightRadius: 10}} resizeMode="stretch"/>
                            <Text style={{marginTop: 15, margin: 8, fontWeight: 'bold', fontSize: 16}}>
                                Pusat Bantuan
                            </Text>
                            <Text style={{margin: 8, fontSize: 13}}>
                                Punya kendala atau pertanyaan terkait OVO? Kamu bisa kirim di sini
                            </Text>
                        </View>
                        <View style={{flex:1, borderRadius: 10, elevation: 2, marginTop: 20, marginStart: 8, marginRight: 23, margin: 5, height: 250, width: 150, backgroundColor: 'white'}}>
                            <Image source={require('../../assets/dummy/covid.jpg')} style={{height: 100, width: "100%", borderTopLeftRadius: 10, borderTopRightRadius: 10}} resizeMode="stretch"/>
                            <Text style={{marginTop: 15, margin: 8, fontWeight: 'bold', fontSize: 16}}>
                                Asuransi COVID-19
                            </Text>
                            <Text style={{margin: 8, fontSize: 13}}>
                                Punya kendala atau pertanyaan terkait OVO? Kamu bisa kirim di sini
                            </Text>
                        </View>
                    </View>
                </View>

                <View style={{flex:1, backgroundColor: '#ededed', height: 13, marginTop: 20}}></View>

            </ScrollView>
    
        </View>
      );
  }
}
