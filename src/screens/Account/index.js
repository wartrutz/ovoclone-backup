import React, { Component } from 'react'
import { View, Text, Image } from 'react-native'

export default class Account extends Component {
    render() {
        return(
            <View style={{flexDirection: "column", flex: 1, backgroundColor: "ffffff"}}>
                <View style={{flexDirection: "row", height: 60, alignSelf: "flex-end", backgroundColor: "ffffff"}}>
                    <Image source={require("../../assets/icons/bell-p.png")} 
                    style={{height: 25,
                    width: 20,
                    marginRight: 17,
                    marginTop: 25}}></Image>
                </View>
                <View style={{flex:1, margin: 15}}>
                    <Text style={{fontSize: 24, fontWeight: "bold"}}>Profile</Text>
                    <View style={{ flexDirection: "row", alignItems: 'center'}}>
                        <Image source={require('../../assets/icons/account.png')} style={{ height: '55%', width: '10%', margin: 10}}></Image>
                        <View>
                            <Text style={{ marginTop: 18, fontWeight: "bold" }}>Enigma Team</Text>
                            <Text style={{ marginBottom: 15}}>0000-0000-0000-0000</Text>
                        </View>
                    </View>
                    <Text>OVO Premiere</Text>
                </View>
            </View>
        )
    }
}