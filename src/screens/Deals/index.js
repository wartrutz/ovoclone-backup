import React, {Component} from 'react';
import { View, Image, Text } from 'react-native';
import styles from '../../styles';

export default class Deals extends Component {
    render() {
        return(
            <View style={{flexDirection: "column", flex: 1}}>
                <View style={{flexDirection: "row", height: 60, backgroundColor: '#4f2494'}}>
                    <Text style={{marginLeft: 19, marginTop: 20, fontSize: 17, fontWeight: "bold", color: "white", flex: 1}}>Deals</Text>
                    <Image source={require("../../assets/icons/bell.png")} style={styles.iconImage}></Image>
                </View>
                <View style={{height: 60, backgroundColor: 'white', alignItems: 'center', justifyContent: 'space-between', flexDirection: "row"}}>
                    <View style={{flex: 1, marginLeft: 19, marginRight: 15, width: 275, height: 40, backgroundColor: '#F2F5F1', borderRadius: 10, justifyContent: 'center', alignItems: 'center'}}>
                        <Text style={{fontSize: 14, color: '#D2D4D1'}}>Cari Merchant</Text>
                    </View>
                    <Image source={require('../../assets/icons/tickets.png')} style={{height: 30, width: 30, marginRight: 19}} />
                </View>
            </View>
        )
    }
} 