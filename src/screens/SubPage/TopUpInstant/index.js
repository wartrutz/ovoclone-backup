import React, { Component } from "react"
import { View, Text, Image, ScrollView } from "react-native"

export default class TopUpInstant extends Component {
    render() {
        return(
            <View style={{flex:1, backgroundColor:'white'}}>
                <ScrollView>
                    <View>
                        <Text style={{ fontWeight: 'bold', fontSize: 16, margin: 15 }}>Top Up ke</Text>
                    </View>
                    <View style={{ justifyContent: "center", alignItems: "center"}}>                   
                        <View style={{ width: '90%', borderRadius: 10, borderWidth: 2, borderColor: '#cccc', flexDirection: "row", alignItems: 'center'}}>
                            <Image source={require('../../../assets/icons/ovo.jpg')} style={{ height: '40%', width: '15%', margin: 10}}></Image>
                            <View>
                                <Text style={{ marginTop: 15, fontWeight: "bold" }}>OVO Cash</Text>
                                <Text style={{ marginBottom: 15}}>Balance Rp0</Text>
                            </View>
                        </View>
                    </View>
                    
                    <View style={{backgroundColor: '#F7F7F7', height: 8, marginTop: 20}}></View>
                    
                    <View style={{ flex:1 }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 16, margin: 15 }}>Pilih Nominal Top Up</Text>
                        <View style={{ flexDirection: "row", justifyContent: "center" }}>
                            <View style={{ width: '30%', borderRadius: 30, borderWidth: 2, borderColor: '#cccc', flexDirection: "row", alignItems: 'center', margin: 3}}>
                                <Text style={{ margin: 5, marginStart: 12 }}>Rp100.000</Text>
                            </View>
                            <View style={{ width: '30%', borderRadius: 30, borderWidth: 2, borderColor: '#cccc', flexDirection: "row", alignItems: 'center', margin: 3}}>
                                <Text style={{ margin: 5, marginStart: 12 }}>Rp200.000</Text>
                            </View>
                            <View style={{ width: '30%', borderRadius: 30, borderWidth: 2, borderColor: '#cccc', flexDirection: "row", alignItems: 'center', margin: 3}}>
                                <Text style={{ margin: 5, marginStart: 12 }}>Rp500.000</Text>
                            </View>
                        </View>
                        <Text style={{ fontSize: 12, marginLeft: 15, marginTop: 8}}>Atau masukkan nominal top up di sini</Text>
                        <View style={{ height: 60, backgroundColor: 'white', alignItems: 'center', justifyContent: 'space-between', flexDirection: "row"}}>
                            <View style={{ marginLeft: 19, width: '90%', height: 40, backgroundColor: '#F2F5F1', borderRadius: 10, justifyContent: 'center', alignItems: 'center'}}>
                                <Text style={{fontSize: 14, color: '#D2D4D1'}}>Minimal Rp100.000</Text>
                            </View>
                        </View>

                        <View style={{backgroundColor: '#F7F7F7', height: 8, marginTop: 10}}></View>

                        <View>
                            <Text style={{ fontWeight: 'bold', fontSize: 16, margin: 15, marginTop: 30, marginBottom: 40 }}>Kartu Debit</Text>
                            <View style={{ height: 60, backgroundColor: 'white', alignItems: 'center', justifyContent: 'space-between', flexDirection: "row"}}>
                                <View style={{ marginLeft: 19, width: '70%', height: 100, backgroundColor: '#F2F5F1', borderRadius: 10, justifyContent: 'center', alignItems: 'center', borderWidth: 2, borderColor: '#1EB9BF'}}>
                                    <Image source={require('../../../assets/icons/debit.png')} style={{ height: 20, width: 28}}></Image>
                                    <Text style={{fontSize: 11, margin: 8, color: '#D2D4D1'}}>Tambahkan kartu debit BCA</Text>
                                </View>
                            </View>
                        </View>
                    </View>

                    <View style={{backgroundColor: '#F7F7F7', height: 8, marginTop: 50}}></View>
                    
                </ScrollView>

                <View style={{elevation: 5, height: 60, backgroundColor: 'white', alignItems: 'center', justifyContent: 'space-between', flexDirection: "row"}}>
                    <View style={{marginLeft: 19, width: '90%', height: 40, backgroundColor: '#F2F5F1', borderRadius: 10, justifyContent: 'center', alignItems: 'center'}}>
                        <Text style={{fontSize: 14, color: 'white', fontWeight: 'bold'}}>Top Up Sekarang</Text>
                    </View>
                </View>
                
            </View>
        )
    }
}