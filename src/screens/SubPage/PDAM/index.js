import React from 'react'
import { View, Text, Image } from 'react-native'

const PDAM = () => {
    return (
        <View style={{flex:1}}>
            <View style={{flex:1, backgroundColor: '#ffffff'}}>
                <Image source={require('../../../assets/icons/patern.png')} style={{ marginTop: 10, width: '100%', height: 85, position: 'absolute' }}/>
                <View style={{flexDirection: 'row', margin: 20, marginTop: 25}}>
                    <Image source={require('../../../assets/icons/pdam.png')} style={{ width: 40, height: 30, marginRight: 10 }} />
                    <Text style={{fontWeight: 'bold', fontSize: 15, marginTop: 5}}>PDAM</Text>
                </View>
                <View style={{height: 60, backgroundColor: 'white', justifyContent: 'space-between'}}>
                    <Text style={{ margin: 10, marginLeft: 19, color: '#979797' }}>Lokasi</Text>
                    <View style={{marginLeft: 19, width: '90%', height: 40, backgroundColor: '#F4F4F4', borderRadius: 10, flexDirection: 'row', justifyContent: 'space-between'}}>
                        <Text style={{fontSize: 14, color: '#D2D4D1', margin: 10}}>Pilih Lokasi</Text>
                        <Text style={{fontSize: 14, color: '#D4D4D4', margin: 10}}>V</Text>
                    </View>
                    <Text style={{ margin: 10, marginLeft: 19, color: '#979797' }}>Nomor Pelanggan</Text>
                    <View style={{marginLeft: 19, width: '90%', height: 40, backgroundColor: '#F4F4F4', borderRadius: 10}}>
                        <Text style={{fontSize: 14, color: '#D2D4D1', margin: 10}}>Contoh 1234567890</Text>
                    </View>
                </View>
            </View>
            <View style={{flex:1}}></View>
            <View style={{elevation: 5, height: 70, backgroundColor: 'white', alignItems: 'center', justifyContent: 'space-between', flexDirection: "row"}}>
                <View style={{marginLeft: 19, width: '90%', height: 40, backgroundColor: '#D4D4D4', borderRadius: 30, justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={{fontSize: 14, color: 'white', fontWeight: 'bold'}}>Lanjut Ke Pembayaran</Text>
                </View>
            </View>
        </View>
    )
}

export default PDAM