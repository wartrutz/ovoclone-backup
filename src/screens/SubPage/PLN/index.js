import React from 'react'
import { Component } from 'react'
import { View, Text, Image } from 'react-native'

export default class PLN extends Component {
    render() {
        return(
            <View style={{flex:1}}>
                <View style={{ backgroundColor: 'white', flexDirection: 'row', alignItems: 'center' }}>
                    <Image source={require('../../../assets/icons/pln.png')} style={{ height: 40, width: 40, margin: 15 }}></Image>
                    <Text style={{ fontWeight: "bold" }}>Token Listrik</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/pln.png')} style={{ height: 40, width: 40, margin: 15 }}></Image>
                    <Text style={{ fontWeight: "bold" }}>Tagihan Listrik</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
            </View>
        )
    }
}
