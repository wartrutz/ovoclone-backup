import React from 'react'
import { View, Text, Image, ScrollView } from 'react-native'

const Voucher = () => {
    return (
        <View style={{flex:1}}>
            <View style={{height: 60, backgroundColor: 'white', alignItems: 'center', justifyContent: 'space-between', flexDirection: "row"}}>
                <View style={{ flex:1, flexDirection: "row", marginLeft: 19, marginRight: 19, width: 320, height: 40, backgroundColor: '#F2F5F1', borderRadius: 10, alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/search.png')} style={{height: 15, width: 15, margin: 10}} />
                    <Text style={{fontSize: 14, color: '#D2D4D1'}}>Cari Voucher Game</Text>
                </View>
            </View>
            <ScrollView>
                <View style={{ flex:1, backgroundColor: 'white', flexDirection: 'row', alignItems: 'center' }}>
                    <Image source={require('../../../assets/icons/battlenet.jpg')} style={{ height: 40, width: 40, margin: 15, borderRadius: 100 }}></Image>
                    <Text style={{ fontWeight: "bold" }}>Battlenet</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{flex:1, borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/bdm.jpg')} style={{ height: 40, width: 40, margin: 15, borderRadius: 100 }}></Image>
                    <Text style={{ fontWeight: "bold" }}>Black Desert Mobile</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{flex:1, borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/codm.jpg')} style={{ height: 40, width: 40, margin: 15, borderRadius: 100 }}></Image>
                    <Text style={{ fontWeight: "bold" }}>Call Of Duty Mobile</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{flex:1, borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/fifa.jpg')} style={{ height: 40, width: 40, margin: 15, borderRadius: 100 }}></Image>
                    <Text style={{ fontWeight: "bold" }}>FIFA Points Voucher</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/ff.jpg')} style={{ height: 40, width: 40, margin: 15, borderRadius: 100 }}></Image>
                    <Text style={{ fontWeight: "bold" }}>Free Fire</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/gs.jpg')} style={{ height: 40, width: 40, margin: 15, borderRadius: 100 }}></Image>
                    <Text style={{ fontWeight: "bold" }}>Gemscool</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/pg.png')} style={{ height: 40, width: 40, margin: 15, borderRadius: 100 }}></Image>
                    <Text style={{ fontWeight: "bold" }}>Kode Voucher Google Play</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/lm.jpg')} style={{ height: 40, width: 40, margin: 15, borderRadius: 100 }}></Image>
                    <Text style={{ fontWeight: "bold" }}>Lords Mobile</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/lyto.png')} style={{ height: 40, width: 40, margin: 15, borderRadius: 100 }}></Image>
                    <Text style={{ fontWeight: "bold" }}>Lyto & Gravindo</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/megaxus.jpeg')} style={{ height: 40, width: 40, margin: 15, borderRadius: 100 }}></Image>
                    <Text style={{ fontWeight: "bold" }}>Megaxus</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/meel.jpg')} style={{ height: 40, width: 40, margin: 15, borderRadius: 100 }}></Image>
                    <Text style={{ fontWeight: "bold" }}>Mobile Legends</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/nintendo.jpg')} style={{ height: 40, width: 40, margin: 15, borderRadius: 100 }}></Image>
                    <Text style={{ fontWeight: "bold" }}>Nintendo eShop</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/ps.png')} style={{ height: 40, width: 40, margin: 15, borderRadius: 100 }}></Image>
                    <Text style={{ fontWeight: "bold" }}>PS Network Card IDR</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/psplus.jpg')} style={{ height: 40, width: 40, margin: 15, borderRadius: 100 }}></Image>
                    <Text style={{ fontWeight: "bold" }}>PS Plus Membership IDR</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/pubgm.jpg')} style={{ height: 40, width: 40, margin: 15, borderRadius: 100 }}></Image>
                    <Text style={{ fontWeight: "bold" }}>PUBG Mobile</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/pubg.jpg')} style={{ height: 40, width: 40, margin: 15, borderRadius: 100 }}></Image>
                    <Text style={{ fontWeight: "bold" }}>PUBG PC Steam Key</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/pb.jpg')} style={{ height: 40, width: 40, margin: 15, borderRadius: 100 }}></Image>
                    <Text style={{ fontWeight: "bold" }}>Point Blank Beyond Limits</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/ro.jpg')} style={{ height: 40, width: 40, margin: 15, borderRadius: 100 }}></Image>
                    <Text style={{ fontWeight: "bold" }}>Ragnarok M Eternal Love</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/razer.png')} style={{ height: 40, width: 40, margin: 15, borderRadius: 100 }}></Image>
                    <Text style={{ fontWeight: "bold" }}>Razer Gold</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/rok.jpg')} style={{ height: 40, width: 40, margin: 15, borderRadius: 100 }}></Image>
                    <Text style={{ fontWeight: "bold" }}>Rise Of Kingdom</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/steam.jpg')} style={{ height: 40, width: 40, margin: 15, borderRadius: 100 }}></Image>
                    <Text style={{ fontWeight: "bold" }}>Steam Wallet IDR</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/unipin.png')} style={{ height: 40, width: 40, margin: 15, borderRadius: 100 }}></Image>
                    <Text style={{ fontWeight: "bold" }}>UniPin Voucher</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/webtoon.png')} style={{ height: 40, width: 40, margin: 15, borderRadius: 100 }}></Image>
                    <Text style={{ fontWeight: "bold" }}>WEBTOON</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/wave.png')} style={{ height: 40, width: 40, margin: 15, borderRadius: 100 }}></Image>
                    <Text style={{ fontWeight: "bold" }}>WaveGame</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/itunes.png')} style={{ height: 40, width: 40, margin: 15, borderRadius: 100 }}></Image>
                    <Text style={{ fontWeight: "bold" }}>iTunes</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
            </ScrollView>
        </View>
    )
}

export default Voucher