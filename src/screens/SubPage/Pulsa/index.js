import React, { Component } from 'react'
import { View, Text, Image } from 'react-native'

export default class Pulsa extends Component {
    render() {
        return(
            <View style={{flex:1, backgroundColor: '#ffffff'}}>
                <Image source={require('../../../assets/icons/patern.png')} style={{ marginTop: 10, width: '100%', height: 85, position: 'absolute' }}/>
                <View style={{flexDirection: 'row', margin: 20, marginTop: 40}}>
                    <Image source={require('../../../assets/icons/operator.png')} style={{ width: 30, height: 30, marginRight: 10 }} />
                    <Text style={{fontWeight: 'bold', fontSize: 15, marginTop: 5}}>Operator</Text>
                </View>
                <View style={{height: 60, backgroundColor: 'white', justifyContent: 'space-between'}}>
                    <Text style={{ margin: 10, marginLeft: 19, color: '#979797' }}>Nomor Ponsel</Text>
                    <View style={{marginLeft: 19, width: '90%', height: 50, backgroundColor: '#EFEBFA', borderRadius: 10}}>
                        <Text style={{fontSize: 16, color: '#CBC7D6', margin: 15}}>Contoh 1234567890</Text>
                    </View>
                </View>
            </View>
        )
    }
}
