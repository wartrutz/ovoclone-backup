import React from 'react'
import { View, Text, Image } from 'react-native'

const TfFavorit = () => {
    return (
        <View style={{flexDirection: "column", flex: 1}}>
            <View style={{marginTop: 80, justifyContent: "center", alignItems: "center"}}>
                <Image source={require("../../../assets/icons/favorit.png")} style={{height: 180, width: 150}} />
                <Text style={{ marginTop: 10, fontSize: 14, color: "#6D6E73" }}>Anda belum punya daftar favorit</Text>
            </View>
        </View>
    )
}

export default TfFavorit