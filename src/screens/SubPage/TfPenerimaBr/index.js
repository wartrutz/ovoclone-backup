import React from 'react'
import { View, Text, Image } from 'react-native'

const TfPenerimaBr = () => {
    return (
        <View style={{flex: 1, flexDirection: 'column'}}>
            <View style={{ flex: 1, margin: 20, marginTop:1, marginBottom:1, justifyContent: "center", alignItems: "center"}}>
                <View style={{ backgroundColor: 'white', width: '100%', height: '30%', borderRadius: 10, flexDirection: "row", alignItems: 'center', elevation: 2, margin: 5}}>
                    <Image source={require('../../../assets/icons/mbank.png')} style={{ height: '40%', width: '9%', margin: 12}}></Image>
                    <View>
                        <Text style={{ textAlign:"center", marginTop: 15, marginBottom: 15, fontWeight: "bold" }}>Internet / Mobile Banking</Text>
                    </View>
                </View>
                <View style={{ backgroundColor: 'white', width: '100%', height: '30%', borderRadius: 10, flexDirection: "row", alignItems: 'center', elevation: 2, margin: 5}}>
                    <Image source={require('../../../assets/icons/gov.png')} style={{ height: '40%', width: '10%', margin: 12}}></Image>
                    <View>
                        <Text style={{ textAlign:"center", marginTop: 15, marginBottom: 15, fontWeight: "bold" }}>ATM</Text>
                    </View>
                </View>
            </View>
            <View style={{backgroundColor: 'white', flex:1}}>
                <View style={{margin: 20}}>
                    <Text style={{fontWeight: 'bold'}}>Transaksi Terakhir</Text>
                    <View style={{marginTop: 30, justifyContent: "center", alignItems: "center"}}>
                        <Image source={require("../../../assets/icons/wallet.png")} style={{height: 100, width: 100}} />
                        <Text style={{ margin: 20, fontSize: 13, textAlign: "center", color: "#D4D4D4" }}>Belum ada transaksi saat ini.</Text>
                    </View>
                </View>
            </View>
        </View>
    )
}

export default TfPenerimaBr