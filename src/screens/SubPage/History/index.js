import React from 'react'
import { View, Text, Image } from 'react-native'

const History = () => {
    return (
        <View style={{flexDirection: "column", flex: 1}}>
            <View style={{marginTop: 80, justifyContent: "center", alignItems: "center"}}>
                <Text style={{ marginTop: 10, fontSize: 18 ,fontWeight: "bold", color: "#402679" }}>View History</Text>
                <Text style={{ marginBottom: 10, fontSize: 14, color: "#6D6E73" }}>Belum ada transaksi</Text>
                <Image source={require("../../../assets/icons/history.png")} style={{height: 65, width: 50}} />
            </View>
        </View>
    )
}

export default History