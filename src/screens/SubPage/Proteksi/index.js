import React from 'react'
import { View, Text, Image, ScrollView } from 'react-native'

const Proteksi = () => {
    return (
        <ScrollView style={{flex:1, backgroundColor: 'white'}}>
            <View style={{flex:1, margin: 20, backgroundColor: 'white'}}>
                <Text style={{fontWeight: "bold", fontSize: 22}}>Nikmati Asuransi Mudah Dengan Premi Terjangkau</Text>
            </View>

            <View style={{flex:1,  backgroundColor: '#ededed', height: 10, marginTop: 1}}></View>
            
            <View style={{flex:1, margin: 10 }}>
                <View style={{flex:1, flexDirection: "row" }}>
                    <Image source={require('../../../assets/icons/checklist.png')} style={{ borderRadius:100, height: 50, width: 50, marginLeft: 10, marginRight: 10, marginTop: 10}}></Image>
                    <View style={{flex:1, marginBottom: 20}}>
                        <Text style={{ marginTop: 15, fontWeight: "bold" }}>Asuransi Kamu</Text>
                        <Text style={{color: '#808080', fontSize: 13}}>Belum ada polis aktif</Text>
                    </View>
                </View>
            </View>

            <View style={{flex:1, backgroundColor: '#ededed', height: 10, marginTop: 1}}></View>

            <View style={{flex:1, margin: 20, marginBottom: 5}}>
                <Text style={{fontSize: 18, fontWeight: 'bold', marginBottom: 15}}>Produk</Text>
                <Image source={require('../../../assets/dummy/27715.jpg')} style={{height: 140, width: '100%', borderRadius: 10, marginBottom: 5}}></Image>
                <Text style={{color: '#808080', marginBottom: 5, fontSize: 12}}>Asuransi - Elektronik</Text>
                <Text style={{fontWeight: "bold", fontSize: 17}}>Proteksi layar smartphone-mu mulai dari Rp.15.000</Text>
            </View>

            <View style={{flex:1, backgroundColor: '#ededed', height: 10, marginTop: 10}}></View>

            <View style={{flex:1, margin: 10}}>
                <View style={{flex:1, flexDirection: "row" }}>
                    <Image source={require('../../../assets/icons/asuransi.jpg')} style={{ borderRadius:100, height: '60%', width: '15%', marginLeft: 10, marginRight: 10, marginTop: 10}}></Image>
                    <View style={{flex:1, marginBottom: 20}}>
                        <Text style={{ marginTop: 15, fontWeight: "bold" }}>Punya Tagihan Premi Asuransi?</Text>
                        <Text style={{color: '#808080', fontSize: 13}}>Bayar di sini aja. Gratis biaya admin!</Text>
                    </View>
                </View>
            </View>

            <View style={{flex:1, backgroundColor: '#ededed', height: 10, marginTop: 1}}></View>

            <View style={{flex:1,  margin: 10}}>
                <View style={{flex:1, flexDirection: "row" }}>
                    <Image source={require('../../../assets/icons/cs.jpg')} style={{ borderRadius:100, height: '60%', width: '15%', marginLeft: 10, marginRight: 10, marginTop: 10}}></Image>
                    <View style={{flex:1, marginBottom: 20}}>
                        <Text style={{ marginTop: 15, fontWeight: "bold" }}>Butuh Bantuan?</Text>
                        <Text style={{color: '#808080', fontSize: 13}}>Kunjungi Pusat Bantuan OVO, yuk!</Text>
                    </View>
                </View>
            </View>

            <View style={{flex:1, backgroundColor: '#ededed', height: 10, marginTop: 1}}></View>
        </ScrollView>
    )
}

export default Proteksi