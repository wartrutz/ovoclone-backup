import React, { Component } from "react"
import { View, Text, ScrollView, Image } from "react-native"

export default class TopUpMetode extends Component {
    render() {
        return(
            <View style={{flex:1}}>
                <View style={{backgroundColor: 'white'}}>
                    <View style={{margin: 35, marginBottom: 20}}>
                        <Text style={{marginBottom: 5, color: '#ADB3B7', fontSize: 13}}>Top Up Saldo Ke</Text>
                        <Text style={{fontSize: 15}}>OVO Cash</Text>
                        <View style={{backgroundColor: 'grey', height: 1, marginTop: 1, marginBottom: 10}}></View>
                        <View style={{ justifyContent: "center", alignItems: "center"}}>                   
                            <View style={{ width: '100%', borderRadius: 10, borderWidth: 2, borderColor: '#cccc', flexDirection: "column", alignItems: 'center'}}>
                                <Text style={{ marginTop: 5, color: '#778189' }}>SALDO OVO CASH</Text>
                                <Text style={{ marginBottom: 5, color: '#6D7780', fontWeight: "bold"}}>Rp0</Text>
                            </View>
                            <Text style={{margin: 5}}>Maks. Saldo OVO Cash Rp10.000.000</Text>
                        </View>
                    </View>
                    <View style={{alignItems: 'center', backgroundColor: '#F8F8F8'}}>
                        <Text style={{margin: 15, color: '#6D7780'}}>Top up makin mudah dengan metode berikut</Text>
                    </View>
                </View>
                <ScrollView style={{backgroundColor: '#F8F8F8'}}>
                    <View style={{margin: 35, marginTop: 10}}>
                        <View style={{ justifyContent: "center", alignItems: "center"}}>                   
                            <View style={{ backgroundColor: 'white', width: '100%', height: '15%', borderRadius: 10, flexDirection: "row", alignItems: 'center', elevation: 2, margin: 5}}>
                                <Image source={require('../../../assets/icons/debit.png')} style={{ height: '40%', width: '11%', margin: 10}}></Image>
                                <View>
                                    <Text style={{ textAlign:"center", marginTop: 15, marginBottom: 15, fontWeight: "bold" }}>OVO Cash</Text>
                                </View>
                            </View>
                            <View style={{ backgroundColor: 'white', width: '100%', height: '15%', borderRadius: 10, flexDirection: "row", alignItems: 'center', elevation: 2, margin: 5}}>
                                <Image source={require('../../../assets/icons/gov.png')} style={{ height: '40%', width: '8%', margin: 12}}></Image>
                                <View>
                                    <Text style={{ textAlign:"center", marginTop: 15, marginBottom: 15, fontWeight: "bold" }}>ATM</Text>
                                </View>
                            </View>
                            <View style={{ backgroundColor: 'white', width: '100%', height: '15%', borderRadius: 10, flexDirection: "row", alignItems: 'center', elevation: 2, margin: 5}}>
                                <Image source={require('../../../assets/icons/mbank.png')} style={{ height: '40%', width: '7%', margin: 12}}></Image>
                                <View>
                                    <Text style={{ textAlign:"center", marginTop: 15, marginBottom: 15, fontWeight: "bold" }}>Internet / Mobile Banking</Text>
                                </View>
                            </View>
                            <View style={{ backgroundColor: 'white', width: '100%', height: '15%', borderRadius: 10, flexDirection: "row", alignItems: 'center', elevation: 2, margin: 5}}>
                                <Image source={require('../../../assets/icons/ovo.jpg')} style={{ height: '40%', width: '11%', margin: 10}}></Image>
                                <View>
                                    <Text style={{ textAlign:"center", marginTop: 15, marginBottom: 15, fontWeight: "bold" }}>OVO Booth</Text>
                                </View>
                            </View>
                            <View style={{ backgroundColor: 'white', width: '100%', height: '15%', borderRadius: 10, flexDirection: "row", alignItems: 'center', elevation: 2, margin: 5}}>
                                <Image source={require('../../../assets/icons/grab.png')} style={{ height: '40%', width: '11%', margin: 10}}></Image>
                                <View>
                                    <Text style={{ textAlign:"center", marginTop: 15, marginBottom: 15, fontWeight: "bold" }}>Grab</Text>
                                </View>
                            </View>
                            <View style={{ backgroundColor: 'white', width: '100%', height: '15%', borderRadius: 10, flexDirection: "row", alignItems: 'center', elevation: 2, margin: 5}}>
                                <Image source={require('../../../assets/icons/tokped.png')} style={{ height: '9%', width: '11%', margin: 10}}></Image>
                                <View>
                                    <Text style={{ textAlign:"center", marginTop: 15, marginBottom: 15, fontWeight: "bold" }}>Tokopedia</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}