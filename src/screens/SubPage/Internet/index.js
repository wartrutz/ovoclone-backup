import React from 'react'
import { View, Text, Image, ScrollView } from 'react-native'

const Internet = () => {
    return (
        <View style={{flex:1}}>
            <ScrollView>
                <View style={{ backgroundColor: 'white', flexDirection: 'row', alignItems: 'center' }}>
                    <Image source={require('../../../assets/icons/biznet.png')} style={{ height: 40, width: 40, margin: 15 }}></Image>
                    <Text style={{ fontWeight: "bold", fontSize: 13 }}>Biznet Home</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/cbn.png')} style={{ height: 40, width: 40, margin: 15 }}></Image>
                    <Text style={{ fontWeight: "bold", fontSize: 13 }}>CBN</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/firstmedia.jpg')} style={{ height: 40, width: 40, margin: 15 }}></Image>
                    <Text style={{ fontWeight: "bold", fontSize: 13 }}>First Media</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/homo.jpg')} style={{ height: 30, width: 40, margin: 15 }}></Image>
                    <Text style={{ fontWeight: "bold", fontSize: 13 }}>IndiHome</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/kv.png')} style={{ height: 30, width: 40, margin: 15 }}></Image>
                    <Text style={{ fontWeight: "bold", fontSize: 13 }}>K-Vision</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/mncp.jpg')} style={{ height: 40, width: 40, margin: 15 }}></Image>
                    <Text style={{ fontWeight: "bold", fontSize: 13 }}>MNC Play</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/mncv.jpg')} style={{ height: 40, width: 40, margin: 15 }}></Image>
                    <Text style={{ fontWeight: "bold", fontSize: 13 }}>MNC Vision</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/myrp.jpg')} style={{ height: 20, width: 40, margin: 15 }}></Image>
                    <Text style={{ fontWeight: "bold", fontSize: 13 }}>My Republic</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
                <View style={{borderTopWidth: 1, borderColor: '#cccc', backgroundColor: 'white', flexDirection: 'row', alignItems: 'center'}}>
                    <Image source={require('../../../assets/icons/tvv.jpg')} style={{ height: 40, width: 40, margin: 15 }}></Image>
                    <Text style={{ fontWeight: "bold", fontSize: 13 }}>Transvision</Text>
                    <View style={{flex:1, flexDirection: "row", justifyContent: "space-between"}}>
                        <View></View>
                        <Image source={require('../../../assets/icons/right-arrow.png')} style={{ height: 15, width: 15, margin: 15, marginRight: 19}}></Image>
                    </View>
                </View>
            </ScrollView>
        </View>
    )
}

export default Internet