import React, {Component} from 'react';
import { View, Image, Text } from 'react-native';
import styles from '../../styles';

export default class Finance extends Component {
    render() {
        return(
            <View style={{flexDirection: "column", flex: 1}}>
                <View style={{flexDirection: "row", height: 60, backgroundColor: '#4f2494'}}>
                    <Text style={{marginLeft: 19, marginTop: 20, fontSize: 17, fontWeight: "bold", color: "white", flex: 1}}>Finance</Text>
                    <Image source={require("../../assets/icons/bell.png")} style={styles.iconImage}></Image>
                </View>
                <View style={{marginTop: 80, justifyContent: "center", alignItems: "center"}}>
                    <Image source={require("../../assets/icons/Finance.png")} style={{height: 150, width: 240}} />
                    <Text style={{ marginTop: 10, fontSize: 18 ,fontWeight: "bold" }}>Kita lagi nyiapin sesuatu</Text>
                    <Text style={{ margin: 15, fontSize: 13, textAlign: "center" }}>Sabar ya, ini semua biar kamu bisa nikmatin berbagai layanan terbaik OVO.</Text>
                </View>
            </View>
        )
    }
} 