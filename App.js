import * as React from 'react'
import { Component } from 'react';
import { View, Text, Image } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'
import { createStackNavigator } from '@react-navigation/stack'

import styles from './src/styles'
import Home from './src/screens/Home'
import Deals from './src/screens/Deals'
import Finance from './src/screens/Finance'
import Account from './src/screens/Account'

import PLN from './src/screens/SubPage/PLN'
import Pulsa from './src/screens/SubPage/Pulsa'
import TopUpInstant from './src/screens/SubPage/TopUpInstant'
import TopUpMetode from './src/screens/SubPage/TopUpMetode'
import TfPenerimaBr from './src/screens/SubPage/TfPenerimaBr';
import TfFavorit from './src/screens/SubPage/TfFavorit';
import History from './src/screens/SubPage/History'
import Voucher from './src/screens/SubPage/VoucherGame';
import PDAM from './src/screens/SubPage/PDAM';
import BPJS from './src/screens/SubPage/BPJS';
import Internet from './src/screens/SubPage/Internet';
import Proteksi from './src/screens/SubPage/Proteksi';


const QRScan = () => {
  return (
    <View style={{flexDirection: "column", flex: 1}}>
      <View style={{flex: 1}}>
      </View>
    </View>
  );
}

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();
const SubTab = createMaterialTopTabNavigator();

const MyTab = () => {

  return (
    <Tab.Navigator tabBarOptions={{
      activeTintColor: '#4f2494',
      style: { height: 58 },
      labelStyle: {
        marginBottom: 5,
        fontSize: 10
      }
    }}
    >
      <Tab.Screen name="Home" component={Home} 
        options={{
          title: "Home",
          tabBarIcon: ({ focused }) => {
              const image = focused
              ? require('./src/assets/icons/home-click.png')
              : require('./src/assets/icons/home.png')
              return (
                <Image
                  source={image}
                  style={styles.navIcon}
                />
              )
          }
        }}
      />
      <Tab.Screen name="Deals" component={Deals} 
        options={{
          title: "Deals",
          tabBarIcon: ({ focused }) => {
            const image = focused
            ? require('./src/assets/icons/deals-click.png')
            : require('./src/assets/icons/deals.png')
            return (
              <Image
                source={image}
                style={styles.navIcon}
              />
            )
          }
        }}
      />
      <Tab.Screen name="QRScan" component={QRScan}
        options={{
          title: "QRScan",
          tabBarIcon: ({ focused }) => {
            const image = focused
            ? require('./src/assets/icons/qrscan.png')
            : require('./src/assets/icons/qrscan.png')
            return (
              <Image
                source={image}
                style={styles.navIconScan}
              />
            )
          }
        }}
      />
      <Tab.Screen name="Finance" component={Finance} 
        options={{
          title: "Finance",
          tabBarIcon: ({ focused }) => {
            const image = focused
            ? require('./src/assets/icons/rupiah-click.png')
            : require('./src/assets/icons/rupiah.png')
            return (
              <Image
                source={image}
                style={styles.navIcon}
              />
            )
          }
        }}
      />
      <Tab.Screen name="Account" component={Account} 
        options={{
          title: "Account",
          tabBarIcon: ({ focused }) => {
            const image = focused
            ? require('./src/assets/icons/account-click.png')
            : require('./src/assets/icons/account.png')
            return (
              <Image
                source={image}
                style={styles.navIcon}
              />
            )
          }
        }}
      />
    </Tab.Navigator>
  );
}

const TopUpTopBar = () => {
  return(
    <SubTab.Navigator
      tabBarOptions={{
        labelStyle: { fontSize: 12, color: 'white', fontWeight: 'bold' },
        style: { backgroundColor: '#4f2494' }
      }} >
      <SubTab.Screen name="TopUpInstant" component={TopUpInstant}
      options = {{
        title: "Instant Top Up"
      }} />
      <SubTab.Screen name="TopUpMetode" component={TopUpMetode}
      options = {{
        title: "Metode Lain"
      }} />
    </SubTab.Navigator>
  )
}

const TfTopBar = () => {
  return(
    <SubTab.Navigator
      tabBarOptions={{
        labelStyle: { fontSize: 12, color: 'white', fontWeight: 'bold' },
        style: { backgroundColor: '#4f2494' }
      }} >
      <SubTab.Screen name="Penerima Baru" component={TfPenerimaBr}/>
      <SubTab.Screen name="Favorit" component={TfFavorit} />
    </SubTab.Navigator>
  )
}

const MyStack = () => {
  return(
    <Stack.Navigator initialRouteName={Home} >
      <Stack.Screen options={{headerShown: false}} name="Home" component={MyTab} />
      <Stack.Screen name="TopUp" 
                    component={TopUpTopBar} 
                    options={{
                      title: "Top Up", 
                      headerStyle: {
                        backgroundColor: '#4f2494',
                        elevation: 0
                      },
                      headerTintColor: 'white'}}  />
      <Stack.Screen name="Transfer" 
                    component={TfTopBar} 
                    options={{ 
                      headerStyle: {
                        backgroundColor: '#4f2494',
                        elevation: 0
                      },
                      headerTintColor: 'white'}}  />
      <Stack.Screen name="History" 
                    component={History} 
                    options={{ 
                      headerStyle: {
                        backgroundColor: '#4f2494',
                        elevation: 0
                      },
                      headerTintColor: 'white'}} />
      <Stack.Screen name="PLN" 
                    component={PLN} 
                    options={{
                      headerStyle: {
                        elevation: 0
                      }
                    }} />
      <Stack.Screen name="Pulsa" 
                    component={Pulsa} 
                    options={{
                      title: "Pulsa & Paket Data",
                      headerTitleStyle: {
                        fontSize: 17
                      },
                      headerStyle: {
                        elevation: 0
                      }
                    }} />
      <Stack.Screen name="Voucher" 
                    component={Voucher} 
                    options={{
                      title: "Voucher Game",
                      headerTitleStyle: {
                        fontSize: 17
                      },
                      headerStyle: {
                        elevation: 0
                      }
                    }} />
      <Stack.Screen name="PDAM" 
                    component={PDAM} 
                    options={{
                      headerTitleStyle: {
                        fontSize: 17
                      },
                      headerStyle: {
                        elevation: 0
                      }
                    }} />
      <Stack.Screen name="BPJS" 
                    component={BPJS} 
                    options={{
                      headerTitleStyle: {
                        fontSize: 17
                      },
                      headerStyle: {
                        elevation: 0
                      }
                    }} />
      <Stack.Screen name="Internet" 
                    component={Internet} 
                    options={{
                      title: "Internet & TV Kabel",
                      headerTitleStyle: {
                        fontSize: 15
                      },
                      headerStyle: {
                        elevation: 0
                      }
                    }} />
      <Stack.Screen name="Proteksi" 
                    component={Proteksi} 
                    options={{
                      title: "OVO | Proteksi",
                      headerTitleStyle: {
                        fontSize: 15,
                        fontWeight: 'bold'
                      },
                      headerStyle: {
                        elevation: 0
                      }
                    }} />
    </Stack.Navigator>
  )
}

export default class Router extends Component {
  
  render() {
    return (
      <NavigationContainer>
        <MyStack />
      </NavigationContainer>
    );
  }
}
